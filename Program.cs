﻿using System;

namespace ChallengeR9
{
    class Program
    {

        static void Main(string[] args)
        {
            int biggestValue;
            String readLines;

            Console.WriteLine("Welcome, how many numbers you are going to enter?\n");
            readLines = Console.ReadLine();
            int[] matrixOfNumbers = new int[Convert.ToInt32(readLines)];
            Console.Clear();

            for (int i = 0; i < matrixOfNumbers.Length; i++)
            {
                Console.WriteLine("Enter the {0} number: ", i+1);
                readLines = Console.ReadLine();
                matrixOfNumbers[i] = Convert.ToInt32(readLines);
                //Console.Clear();
            }

            Console.WriteLine("\nWe are ready to begin the analysis.\n\nPress any key to continue.");
            Console.ReadKey();
            Console.Clear();
            biggestValue = GetSumFromMatrix(matrixOfNumbers);

            if (biggestValue <= -1000000000)
            {
                Console.WriteLine("The result is: 0\n(no ascendent numbers detected)\n\nPress any key to exit.");
            }
            else
            {
                Console.WriteLine("The result is: {0}\n\nPress any key to exit.", biggestValue);
            }

            Console.ReadKey();
        }

        static int GetSumFromMatrix(int[] randomNumbers)            //Remember, is static so it doesn't need an object of "Program" class
        {
            int auxAndResult = 0;
            int[] sumsFromMatrix = new int[randomNumbers.Length / 2];

            for (int i = 0; i < sumsFromMatrix.Length; i++)
            {
                sumsFromMatrix[i] = -1000000000;
            }

            for (int i = 1; i < randomNumbers.Length; i++)
            {
                if (randomNumbers[i] >= randomNumbers[i - 1])
                {
                    sumsFromMatrix[auxAndResult] = randomNumbers[i] + randomNumbers[i - 1];

                    for (int j = i+1; j < randomNumbers.Length; j++)
                    {
                        if (randomNumbers[j] >= randomNumbers[j - 1])
                        {
                            sumsFromMatrix[auxAndResult] += randomNumbers[j];
                        }
                        else
                        {
                            auxAndResult++;
                            i = j - 1;
                            break;
                        }
                    }
                }
            }

            auxAndResult = -1000000000;
            for (int i = 0; i < sumsFromMatrix.Length; i++)
            {
                if (sumsFromMatrix[i] > auxAndResult)
                {
                    auxAndResult = sumsFromMatrix[i];
                }
            }

            return auxAndResult;
        }
    }
}